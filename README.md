# Global variables defined in /local-inc/config
`ARCH` - processor architecture, one of: '' | amd64 | arm64 | armhf | i386 | ppc64el | riscv64 | s390x

`NPROC` - number of processing units available

`SNAP_DIR` - absolute path to the snap directory

`CONFIG_DIR` - absolute path to the directory containing VMs' configuration files

`STORAGE_DIR` - absolute path to the VMs' data storage

`WGET_USER` - username used to run wget

`QEMU_USER` - username used to run qemu

`QEMU_LIB_DIR` - absolute path to the QEMU ROMs (optional)

# Global variables defined in /vmm
`SCR_NAME` - name of a currently running script

`SCR_DIR` - absolute path to a directory where running script located

`INC_DIR` - absolute path to a directory containing include files

`LOCAL_INC_DIR` - absolute path to a directory containing include files not managed by git

`VM_NUM` - vm number if we managing a VM

`VM_BASE` - name of a base which VM uses

`VM_CMT` - comment for VM

`CMD` - executing command

`HELP_MSG` - string containing help message

# Global functions
`warn <message>` - print a warning

`die <message>` - print an error message and exit with an error code

`inc <include file1>...` - include scripts from `LOCAL_INC_DIR` or `INC_DIR`

# Tips for creating your own VM description
Every VM description should include `vm-lib`. If VM uses snap packages, it is also necessary to include `snap-lib`.

VM properties are described by optional "feature" variables:

`FEAT_CONSOLES` - number of consoles to allocate (consoles are disabled by default)

`FEAT_MEM_LIMIT` - memory limit in MiB, also supports fractional part and G suffix

`FEAT_TMP` - enables tmpfs for VM data. Contains tmpfs size, may have k, m, or g suffix

`FEAT_NET` - value 1 enables network (by default, network is disabled)

And by other global variables:

`VM_CONFIG_DIR` - absolute path to a VM config directory

`VM_STORAGE` - absolute path to a directory where VM data is stored

`VM_UP` - absolute path to upperdir for overlayfs

`VM_WRK` - absolute path to workdir for overlayfs

`VM_ROOT` - absolute path to VM's root filesystem

You can also add your own functions, including redefinition of `_mount`, `_run`, etc. from `vm-lib`. But note that a common design is to check before execution if any action is really needed and to run function dependencies at the beginning.
